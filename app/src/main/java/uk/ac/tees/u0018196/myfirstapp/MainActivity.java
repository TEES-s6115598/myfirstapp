package uk.ac.tees.u0018196.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "uk.ac.tees.s6115598.EXTRA_MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void displayText(View v)
    {
        EditText editText = findViewById(R.id.editText);
        Intent intent = new Intent(this, DisplayTextActivity.class);
        String text = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, text);
        startActivity(intent);
    }
}
